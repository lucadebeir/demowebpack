const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const dev = process.env.NODE_ENV === "dev"

let cssLoaders = [
    {
        loader: 'css-loader', 
        options: { 
            importLoaders: 1
        } 
    }
]

if(!dev) {
    cssLoaders.push({
        loader: 'postcss-loader',
        options: {
            postcssOptions: {
                plugins: (loader) => [
                    require('autoprefixer')(
    
                    )
                ]
            }
        }
    })
}

let config = {
    //fichier d'entrée
    mode: 'development',
    entry: {
        app: ['./assets/css/app.scss', './assets/js/app.js']
    },
    watch: dev,

    output: {
        path: path.resolve('./public/assets/'),
        filename: dev ? '[name].js' : '[name].[chunkhash:8].js',
        publicPath: (dev ? 'http://localhost:8080' : '') + '/assets/'
    },
    resolve: {
        alias: {
            '@css': path.resolve('./assets/css/'),
            '@': path.resolve('./assets/js/')
        }
    },
    devtool: dev ? 'eval-cheap-module-source-map' : "source-map",
    devServer: {
        contentBase: path.resolve('./public'),
        overlay: true,
        proxy: {
            '/web': {
              target: 'http://localhost:8000',
              pathRewrite: { '^/web': '' },
            },
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'X-Requested-With, content-type',
        },
        //hot: true
    },
    module: {
        rules: [
            //eslint
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['eslint-loader']
            },
            //babel
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            },
            //css
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, ...cssLoaders]
            },
            //scss
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, ...cssLoaders, 'sass-loader']
            },
            //font
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'file-loader'
            },
            //image
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                  {
                    loader: 'url-loader',
                    options: {
                      limit: 8192,
                      name: '[name].[hash:7].[ext]'
                    },
                  },
                  {
                      loader: 'img-loader',
                      options: {
                        enabled: !dev
                      }
                  }
                ],
              },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: dev ? '[name].css' : '[name].[contenthash:8].css'
        }),
        new HtmlWebpackPlugin()
    ]
}

if(!dev) {
    config.plugins.push(new UglifyJSPlugin({
        sourceMap: true
    }))
    config.plugins.push(new WebpackManifestPlugin())
    config.plugins.push(new CleanWebpackPlugin({
        root: path.resolve('./'),
        verbose: true,
        dry: false
    }))
}

module.exports = config;